### 第三周学习

#### 一。跳转到新页面

```

```

小程序使用

   

```
wx.navigate To(Object)
```

保留当前页面，并在当前页面上方打开应用内指定的新页面。在这种打开方式下可以单击新页面左上角的返回按钮或使用

```
wx.navigateBack()

```

```
参数
url String 需要跳转的应用内非tabBar的页面路径，路径后可以带参数。参数与路径之间使用？分隔，参数键与参数值用=相连，多个参数用&分隔，例:’path？key=value&key2=value2'
success() Fuction 接口调用成功的回调函数
fail()  Fuction 接口调用失败的回调函数
complete() Fuction 接口调用结束的回调函数
```



#### 二.返回指定页面

```
小程序使用wx.navigateBack(Object )关闭当前页面，返回上一页面或多级页面
参数 delta Number 默认值 1 返回的页面数，如果delta大于现有页面数 ，则返回到首页
```

#### 三.当前页面重定向

```
小程序使用wx.redirectTo(Obiect)关闭当前页面内容，重定向到应用内某个页面
```

